import random
import numpy as ny



def losuj_macierz(macierz):
    for i in range(len(macierz[1])):
        for j in range(len(macierz[1])):
            macierz[i,j] = random.randrange(10)
    return macierz

def pomnoz_macierze(macierz1,macierz2):
    wynikowa = ny.ones((len(macierz1),len(macierz1)))
    wynikowa = ny.dot(macierz1,macierz2)
    return wynikowa
macierz1 = ny.ones((8,8))
macierz2 = ny.ones((8,8))
macierz1 = losuj_macierz(macierz1)

print("macierz 1: ")
print(macierz1)
macierz2 = losuj_macierz(macierz2)
print("macierz 2: ")
print(macierz2)
print("macierz 1 x macierz 2: ")
print(pomnoz_macierze(macierz1,macierz2))