import random
import numpy as ny

def losuj_macierz(macierz):
    for i in range(len(macierz[1])):
        for j in range(len(macierz[1])):
            macierz[i,j] = random.randrange(10)
    return macierz

def dodaj_macierze(macierz1,macierz2):
    if len(macierz1) == len(macierz2):
        result = ny.ones((len(macierz1),len(macierz1)))
        for i in range(len(macierz1)):
            for j in range(len(macierz1)):
                result[i,j] = macierz1[i,j]+macierz2[i,j]
        return result
    else:
        return "ERROR"

macierz1 = ny.ones((128,128))
macierz2 = ny.ones((128,128))
macierz1 = losuj_macierz(macierz1)
macierz2 = losuj_macierz(macierz2)

print(dodaj_macierze(macierz1,macierz2))

