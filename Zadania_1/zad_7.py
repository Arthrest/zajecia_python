import math


def function(a,b,c):
    if a == 0:
        print("to nie jest funkcja kwadratowa!")
        exit()
    else:
        delta = b*b - 4*a*c
        if delta > 0:
            x1 = (((-1)*b) + math.sqrt(delta)) / (2 * a)
            x2 = (((-1) * b) - math.sqrt(delta)) / (2 * a)
            return x1, x2
        else:
            print("program nie przewiduje pracy z liczbami urojonymi")


a = input("wprowadz a: ")
b = input("wprowadz b: ")
c = input("wprowadz c: ")
print("oblieczenie funkcji kwadratowej z wybranego wielomianu: ")
print(function(int(a), int(b), int(c)))