import numpy
import random
from numpy import linalg

def losuj_macierz(macierz):
    for i in range(len(macierz[1])):
        for j in range(len(macierz[1])):
            macierz[i,j] = random.randrange(10)
    return macierz

def licz_wyznacznik(macierz):
    result = numpy.linalg.det(macierz)
    return result

macierz1 = numpy.ones((8,8))
macierz1 = losuj_macierz(macierz1)
print("macierz: ")
print(macierz1)
print("wyznacznik powyższej macierzy: ")
print(licz_wyznacznik(macierz1))
