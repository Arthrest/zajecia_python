import threading
import time
import sys

# declaration of five forks, 0 mean free, 1 - taken
forks_lock =  threading.Semaphore()
forks = [0,0,0,0,0]


'''
Klasa reprezentuje wątek-filozofa stwarzający ryzyko zakleszczenia.
Każdy z tego typu wątków próbuje zabrać zasób, czyli odpowiedniedwa widelce (forks).
Jeśli uda mu się zabrać jeden, nie zwalnia go dopóki nie uda mu się zdobyć kolejnego.
To stwarza ryzyko zakleszczenia, gdy wszystkie wątki zdobędą po 1 widelec.
'''
class PhilosopherWithDeadlock(threading.Thread):

    def __init__(self, number):
        self.number = number
        self.goOn = True
        self.fork_counter = 0
        print("Philosopher number " + str(self.number) + " has entered to dining room.")
        threading.Thread.__init__(self)

    def run(self):
        global forks
        while self.goOn:
            time.sleep(5)
            forks_lock.acquire()
            if forks[self.number] == 0:
                print("Philosopher number " + str(self.number) + " has got first fork.")
                forks[self.number] = 1
                self.fork_counter += 1
            if self.number == 4:
                if forks[0] == 0:
                    print("Philosopher number " + str(self.number) + " has got second fork.")
                    forks[0] = 1
                    self.fork_counter += 1
            else:
                if forks[self.number+1] == 0:
                    print("Philosopher number " + str(self.number) + " has got second fork.")
                    forks[self.number+1] = 1
                    self.fork_counter += 1
            forks_lock.release()

            if self.fork_counter == 2:
                print("Philosopher number " + str(self.number) + " can eat.")
                time.sleep(5)
                forks_lock.acquire()
                self.fork_counter = 0
                if self.number == 4:
                    forks[self.number] = 0
                    forks[0] = 0
                else:
                    forks[self.number] = 0
                    forks[self.number+1] = 0
                forks_lock.release()



    def close(self):
        self.goOn = False

'''
Klasa reprezentuje ulepszoną klasę wątku-filozofa. W tym wypadku każdy wątek, który
zdobędzie jeden widelec, próbuje zdobyć kolejny przez pewien czas (tutaj 15s). Jeśli mu się
to nie uda, zwalnia zasob, który blokuje, by nie doprowadzić do zakleszczenia.
'''

class PhilosopherWithoutDeadlock(threading.Thread):

    def __init__(self, number):
        self.number = number
        self.goOn = True
        self.fork_counter = 0
        self.timeout = 15
        self.reference_time = 0
        self.first_fork = 9
        self.second_fork = 9
        print("Philosopher number " + str(self.number) + " has entered to dining room.")
        threading.Thread.__init__(self)

    def run(self):
        global forks
        while self.goOn:
            time.sleep(5)
            forks_lock.acquire()
            if forks[self.number] == 0:
                print("Philosopher number " + str(self.number) + " has got first fork.")
                forks[self.number] = 1
                self.first_fork = self.number
                self.fork_counter += 1
                self.reference_time = time.time()
            if self.number == 4:
                if forks[0] == 0:
                    print("Philosopher number " + str(self.number) + " has got second fork.")
                    forks[0] = 1
                    self.second_fork = 0
                    self.fork_counter += 1
                    self.reference_time = time.time()
            else:
                if forks[self.number+1] == 0:
                    print("Philosopher number " + str(self.number) + " has got second fork.")
                    forks[self.number+1] = 1
                    self.second_fork = self.number + 1
                    self.fork_counter += 1
                    self.reference_time = time.time()
            forks_lock.release()

            if self.fork_counter == 2:
                self.reference_time = 0
                print("Philosopher number " + str(self.number) + " can eat.")
                time.sleep(5)
                forks_lock.acquire()
                self.fork_counter = 0
                self.first_fork = 9
                self.second_fork = 9
                if self.number == 4:
                    forks[self.number] = 0
                    forks[0] = 0
                else:
                    forks[self.number] = 0
                    forks[self.number+1] = 0
                forks_lock.release()
            else:
                if self.reference_time > self.timeout:
                    if not(self.first_fork == 9):
                        forks_lock.acquire()
                        forks[self.first_fork] = 0
                        self.fork_counter = 0
                        self.first_fork = 9
                        forks_lock.release()
                    if not(self.second_fork == 9):
                        forks_lock.acquire()
                        forks[self.second_fork] = 0
                        self.fork_counter = 0
                        self.second_fork = 9
                        forks_lock.release()

    def close(self):
        self.goOn = False

def main():

    philo0 = PhilosopherWithDeadlock(0)
    philo1 = PhilosopherWithDeadlock(1)
    philo2 = PhilosopherWithDeadlock(2)
    philo3 = PhilosopherWithDeadlock(3)
    philo4 = PhilosopherWithDeadlock(4)

    philo0.start()
    philo1.start()
    philo2.start()
    philo3.start()
    philo4.start()

    input("Enter to stop dining philosophers with deadlock test")

    philo0.close()
    philo1.close()
    philo2.close()
    philo3.close()
    philo4.close()

    philo0.join()
    philo1.join()
    philo2.join()
    philo3.join()
    philo4.join()

    time.sleep(1)

    forks[0] = 0
    forks[1] = 0
    forks[2] = 0
    forks[3] = 0
    forks[4] = 0

    phil0 = PhilosopherWithoutDeadlock(0)
    phil1 = PhilosopherWithoutDeadlock(1)
    phil2 = PhilosopherWithoutDeadlock(2)
    phil3 = PhilosopherWithoutDeadlock(3)
    phil4 = PhilosopherWithoutDeadlock(4)

    phil0.start()
    phil1.start()
    phil2.start()
    phil3.start()
    phil4.start()

    input("Enter to stop dining philosophers without deadlock test")

    phil0.close()
    phil1.close()
    phil2.close()
    phil3.close()
    phil4.close()

    phil0.join()
    phil1.join()
    phil2.join()
    phil3.join()
    phil4.join()

    # wait for a while to all threads stop
    time.sleep(1)

    sys.exit(0)


if __name__ == '__main__':
    main()
