import psutil
import threading
import time
import os

# method printing bars of histogram
def print_histogram(data):
    # clear console window
    os.system("cls")
    for x in range(len(data)):
        hist = ""
        eval = int(data[x])
        y=0
        if eval == 0:
            hist += " "
        # add proper number of bars
        for y in range(eval):
            hist += "|"
        # add proper number of spaces
        for z in range(100-y):
            hist += " "
        # print statistic as a number too
        hist += str(data[x]) + "%"
        print("Core" + str(x) + " " + hist)


cpu_stats = []


# thread used for printing bars of cpu percent usage
class PlotHistogram(threading.Thread):
    def __init__(self):
        self.goOn = True
        threading.Thread.__init__(self)

    def run(self):
        global cpu_stats
        while self.goOn:
            time.sleep(0.5)
            print_histogram(cpu_stats)

    def close(self):
        self.goOn = False


# thread used for calculating cpu's cores percent usage
class CPUMonitor(threading.Thread):
    def __init__(self):
        self.goOn = True
        threading.Thread.__init__(self)

    def run(self):
        global cpu_stats
        while self.goOn:
            time.sleep(0.1)
            # calculate cpu statistics (as an array of values for each core)
            cpu_stats = psutil.cpu_percent(0.5, percpu=True)

    def close(self):
        self.goOn = False

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# application has to run from system console, not python one
# (because it uses sys("cls") to refresh window
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def main():

    plot = PlotHistogram()
    cpu = CPUMonitor()

    plot.start()
    cpu.start()

    input("Enter to stop application")

    plot.close()
    cpu.close()

    plot.join()
    cpu.join()


if __name__ == '__main__':
    main()
