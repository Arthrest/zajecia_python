import numpy


class Complex:
    def __init__(self, real, complex):
        self.r = real
        self.i = complex

    def printComplex(self):
        if self.i >= 0:
            print(("{:2.2f} + {:2.2f}i").format(self.r, self.i))
        else:
            print(("{:2.2f} - {:2.2f}i").format(self.r, (-1) * self.i))

    def addComplex(a, b):
        result = Complex(0, 0)
        result.r = a.r + b.r
        result.i = a.i + b.i
        return result

    def subtractComplex(a, b):
        result = Complex(0, 0)
        result.r = a.r - b.r
        result.i = a.i - b.i
        return result

    def absComplex(a):
        return numpy.sqrt((a.r * a.r) + (a.i * a.i))

    def trygonometricComplex(a):
        fi = 0
        if a.i > 0 and Complex.absComplex(a) != 0:
            fi = numpy.arccos(a.r/Complex.absComplex(a))
        elif a.i < 0:
            fi = -numpy.arccos(a.r/Complex.absComplex(a))
        abs = Complex.absComplex(a)
        fi = numpy.rad2deg(fi)

        if fi < 0:
            print("{:2.2f}[-cos({:2.2f}) - isin({:2.2f})]".format(abs, (-1)*fi, (-1)*fi))

        else:
            print("{:2.2f}[cos({:2.2f}) + isin({:2.2f})]".format(abs, fi, fi))

    def argComplex(a):
        fi = 0
        if a.i > 0 and Complex.absComplex(a) != 0:
            fi = numpy.arccos(a.r/Complex.absComplex(a))
        elif a.i < 0:
            fi = -numpy.arccos(a.r/Complex.absComplex(a))
        return fi

    def multiplyComplex(a, b):
        wynik.r = (a.r * b.r) + (-1)*(a.i * b.i)
        wynik.i = (a.r * b.i) + (b.r * a.i)
        return wynik

x = Complex(5, -3)
print("pierwsza liczba zespolona: ")
x.printComplex()

y = Complex(6, 4)
print("druga liczba zespolona: ")
y.printComplex()

wynik = Complex.addComplex(x, y)
print("wynik dodawania liczb zespolonych: ")
wynik.printComplex()

wynik = Complex.subtractComplex(x, y)
print("wynik odejmowania liczb zespolonych: ")
wynik.printComplex()

absX = Complex.absComplex(x)
print("modul liczby zespolonej to: {:2.2f}".format(absX))

print("argument liczby zesplonej to: {:2.2f}".format(Complex.argComplex(x)))

print("postac trygonometryczna liczby numer 1: ")
Complex.trygonometricComplex(x)

wynik2 = Complex.multiplyComplex(x,y)
print("wynik mnozenia dwoch liczb zespolonych:")
wynik2.printComplex()





