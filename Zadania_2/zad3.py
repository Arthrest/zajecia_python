import xml.sax
from typing import Type
from xml.dom.minidom import parse
import xml.sax


class parseUsingDOM:

    path = ""

    def __init__(self, path):
        self.path = path
        try:
            messanger = parse(self.path)
            self.messageNodes = messanger.childNodes
        except IOError as e:
            print("bledna sciezka do pliku xml")

    def printAllNodes(self):
        j = 0
        for i in self.messageNodes[0].getElementsByTagName("message"):
            j += 1
            print("--------------------------------------------------------")
            print("message nr {}".format(j))
            # wartosc taga:
            print("message to: " + i.getElementsByTagName("to")[0].childNodes[0].toxml())
            print("message from: " + i.getElementsByTagName("from")[0].childNodes[0].toxml())
            print("header: " + i.getElementsByTagName("head")[0].childNodes[0].toxml())
            print("Body: " + i.getElementsByTagName("body")[0].childNodes[0].toxml())
            print("--------------------------------------------------------")

    def changeNode(self, node, No, newValue):
        try:
            self.messageNodes[0].getElementsByTagName(node)[No].childNodes[No].nodeValue = newValue
        except IOError as e:
            print("sciezka niepoprawna sprobuj pownownie!")

class SAXHandler(xml.sax.ContentHandler):

    def __init__(self):
        self.CurrentData = ""
        self.xml_to = ""
        self.xml_from = ""
        self.xml_head = ""
        self.xml_body = ""

    def startElement(self, tag, attr):
        self.CurrentData = tag
        if tag == "message":
            print("-------------------------------------")
            title = attr["type"]
            print("Message type: " + title)
    def endElement(self, tag):
        if self.CurrentData == "to":
            print("To:   ", self.xml_to)
        elif self.CurrentData == "from":
            print("From: ", self.xml_from)
        elif self.CurrentData == "head":
            print("Head: ", self.xml_head)
        elif self.CurrentData == "body":
            print("Body: ", self.xml_body)
        self.CurrentData = ""
    def characters(self, content):
        if self.CurrentData == "to":
            self.xml_to = content
        elif self.CurrentData == "from":
            self.xml_from = content
        elif self.CurrentData == "head":
            self.xml_head = content
        elif self.CurrentData == "body":
            self.xml_body = content

class parseUsingSAX:
    def __init__(self, path):
        parser = xml.sax.make_parser()
        parser.setFeature(xml.sax.handler.feature_namespaces, 0)
        handler = SAXHandler()
        parser.setContentHandler(handler)
        try:
            parser.parse(path)
        except IOError as e:
            print("bledna sciezka do pliku")


print("\n\nsparsowane DOM przed zmiana: ")
DOM_parser = parseUsingDOM("xmlSample.xml")
DOM_parser.printAllNodes()
print("\n\nsparsowane DOM po zmianie: ")
DOM_parser.changeNode("to",0,"Andrzej")
DOM_parser.printAllNodes()

print("\n\nsparsowane SAX przed zmiana: ")
SAX_test = parseUsingSAX("xmlSample.xml")